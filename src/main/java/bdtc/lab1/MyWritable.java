package bdtc.lab1;

import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Objects;

public class MyWritable implements Writable {
    private int count;
    private String str;

    public  MyWritable()
    {}

    public MyWritable(int count, String str)
    {
        this.count = count;
        this.str = str;
    }

    @Override
    public void write(DataOutput out) throws IOException {
        out.writeInt(count);
        out.writeUTF(str);
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        count = in.readInt();
        str = in.readUTF();
    }

    @Override
    public String toString() {
        return count +", " + str;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MyWritable that = (MyWritable) o;
        return count == that.count && Objects.equals(str, that.str);
    }

    @Override
    public int hashCode() {
        return Objects.hash(count, str);
    }
}
