package bdtc.lab1;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.log4j.pattern.LineSeparatorPatternConverter;

import java.io.IOException;
import java.util.*;

/**
 * Mapper: составляет ключ (metricName,timestamp,scale), значение (value)
 */

public class HW1Mapper extends Mapper<LongWritable, Text, Text, IntWritable>
{

    private Text word = new Text();



    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {

        FillMap();
        String line = value.toString();
        String[] strings = line.split(";");
        if(strings.length!=4) {
            context.getCounter(CounterType.MALFORMED).increment(1);
            return;
        }

        for (String item: strings) {
            if (item.isEmpty() ) {
                context.getCounter(CounterType.MALFORMED).increment(1);
                return;
            }
        }

        try
        {
            int x = Integer.parseInt(strings[0].trim());
            int y = Integer.parseInt(strings[1].trim());
            long userId = Long.parseLong(strings[2].trim());

            MyCoordinate coord = new MyCoordinate(x,y);
            String myDicScreen ="";
            for(Map.Entry<String, List<MyCoordinate>> entry: Regions.entrySet())
            {
                String mapKey = entry.getKey();
                List<MyCoordinate> mapList = entry.getValue();
                if (mapList.contains(new MyCoordinate(x,y))) {
                    myDicScreen = mapKey;
                    break;
                }

            }
            if (myDicScreen=="")
            {
                myDicScreen=DicScreen[0];
            }
            // ключ будет составлять из:
            // -названия экрана
            word.set(myDicScreen);

            context.write(word, new IntWritable(1));
        }
        catch (IOException e)
        {
            context.getCounter(CounterType.MALFORMED).increment(1);
        }

    }

    // справочник Областей экрана: DicScreen
    private static final String[] DicScreen = {"None","Button-Ok","Exit","Close"};
    private static final Map<String, List<MyCoordinate>> Regions = new HashMap<String,List<MyCoordinate>>();

    private  static void  FillMap()
    {
        Regions.clear();
        List<MyCoordinate> Coordinates1 = new ArrayList<>();
        Coordinates1 = AddRectangle(new MyCoordinate(0,0), new MyCoordinate(10,10));
        Regions.put(DicScreen[1], Coordinates1);

        List<MyCoordinate> Coordinates2 = new ArrayList<>();
        Coordinates2 = AddRectangle(new MyCoordinate(0,11), new MyCoordinate(21,21));
        Coordinates2 = AddRectangle(new MyCoordinate(11,0), new MyCoordinate(21,21));
        Regions.put(DicScreen[2], Coordinates2 );

        List<MyCoordinate> Coordinates3 = new ArrayList<>();
        Coordinates3 = AddRectangle(new MyCoordinate(22,22), new MyCoordinate(55,55));
        Regions.put(DicScreen[3], Coordinates3 );
    }

    private static List<MyCoordinate>  AddRectangle(MyCoordinate begin, MyCoordinate end)
    {
        List<MyCoordinate> Coordinates = new ArrayList<>();
        for (int x= begin.X; x<=end.X; x++)
        {
            for (int y= begin.Y; y<=end.Y; y++)
            {
                Coordinates.add(new MyCoordinate(x,y));
            }
        }

        return  Coordinates;
    }

    private static class MyCoordinate
    {
        int X;
        int Y;

        public  MyCoordinate(int x, int y)
        {
            X=x;
            Y=y;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            MyCoordinate that = (MyCoordinate) o;
            return X == that.X && Y == that.Y;
        }

        @Override
        public int hashCode() {
            return Objects.hash(X, Y);
        }
    }
}
