
import bdtc.lab1.HW1Mapper;
import bdtc.lab1.HW1Reducer;
import bdtc.lab1.MyWritable;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class MapReduceTest {

    private MapDriver<LongWritable, Text, Text, IntWritable> mapDriver;
    private ReduceDriver<Text, IntWritable, Text, MyWritable> reduceDriver;
    private MapReduceDriver<LongWritable, Text, Text, IntWritable, Text, MyWritable> mapReduceDriver;

    private final String testNotOk = "mama mila ramu";
    private final String testOK= "100; 100; 926; 1612193736";


    @Before
    public void setUp()
    {
        HW1Mapper mapper = new HW1Mapper();
        HW1Reducer reducer = new HW1Reducer();
        mapDriver = MapDriver.newMapDriver(mapper);
        reduceDriver = ReduceDriver.newReduceDriver(reducer);
        mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);
    }

    @Test
    public void testMapper() throws IOException {
        mapDriver
                .withInput(new LongWritable(), new Text(testOK))
                .withOutput(new Text("None"), new IntWritable(1))
                .runTest();
    }

    @Test
    public void testReducer() throws IOException {
        List<IntWritable> values = new ArrayList<IntWritable>();
        values.add(new IntWritable(40));
        values.add(new IntWritable(60));
        reduceDriver
                .withInput(new Text("None"), values)
                .withOutput(new Text("None"), new MyWritable( 100,"Низкое количество нажатий."))
                .runTest();
    }

    @Test
    public void testMapReduce() throws IOException {
        mapReduceDriver
                .withInput(new LongWritable(), new Text(testOK))
                .withInput(new LongWritable(), new Text(testNotOk))
                .withOutput(new Text("None"), new MyWritable( 1,"Низкое количество нажатий."))
                .runTest();
    }
}